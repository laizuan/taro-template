import { createApp } from 'vue';
import Taro from '@tarojs/taro';
import stores from './stores/index';
import { Button, Toast, Tabbar, TabbarItem, Icon } from '@nutui/nutui-taro';
import '@nutui/nutui-taro/dist/styles/themes/default.scss';
//@ts-ignore
import Wrap from '@/components/Wrap/index';

import './app.scss';

const App = createApp({
  onShow(_options) {
    Taro.login({
      success(res) {
        if (res.code) {
        } else {
          console.log(`登录失败！${res.errMsg}`);
        }
      }
    });
  },
  // 对应 onLaunch
  onLaunch(_options) {}
});

App.use(stores).use(Button).use(Tabbar).use(TabbarItem).use(Icon).use(Toast).component('Wrap', Wrap);

export default App;
