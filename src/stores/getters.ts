const getters = {
  loginState: (state: any) => state.app.loginState,
  loginUser: (state: any) => state.app.loginUser,
};
export default getters;
