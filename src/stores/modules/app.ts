// @ts-ignore
import { TabbarRoute } from '@/components/Wrap/tabbar-router';
import { Commit } from 'vuex';

const app = {
  state: {
    loginState: false,
    loginUser: {},
    tabbarMenu: [] as TabbarRoute[],
  },
  mutations: {
    SET_LOGIN_STATE: (state: any, loginState: boolean) => {
      state.loginState = loginState;
    },
    SET_LOGIN_USER: (state: any, loginUser: StringObject) => {
      state.loginUser = loginUser;
    },
  },
  actions: {
    setLoginUser(context: { commit: Commit }, loginUser: StringObject) {
      context.commit('SET_LOGIN_USER', loginUser);
    },
    setLoginState(context: { commit: Commit }, loginState: boolean) {
      context.commit('SET_LOGIN_STATE', loginState);
    },
  },
  getters: {},
};
export default app;
