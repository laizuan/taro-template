export default {
  pages: ['pages/home/index', 'pages/my/index'],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  }
  // tabBar: {
  //   color: '#7d7e80',
  //   borderStyle: 'white',
  //   selectedColor: '#1989fa',
  //   backgroundColor: '#fff',
  //   list: [
  //     { pagePath: 'pages/home/index', text: '首页' },
  //     { pagePath: 'pages/my/index', text: '我的' }
  //   ]
  // }
};
