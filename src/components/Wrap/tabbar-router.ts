import { TabbarRoute } from '../../types/index';
export const routerList: TabbarRoute[] = [
  {
    routeName: 'Home',
    title: '首页',
    icon: 'home',
    url: '/pages/home/index',
  },
  {
    routeName: 'Category',
    title: '分类',
    icon: 'category',
    url: '/pages/category/index',
  },
  {
    routeName: 'My',
    title: '我的',
    icon: 'my',
    url: '/pages/my/index',
  },
];
