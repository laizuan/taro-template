import Taro from '@tarojs/taro';

export const useRouter = () => {
  /**
   * 跳转路由
   * @param  url路径 {string}
   */
  const push = (url: string, option?: Taro.redirectTo.Option) => {
    Taro.navigateTo({
      url,
      ...option
    });
  };

  const redirect = (url: string, option?: Taro.redirectTo.Option) => {
    Taro.redirectTo({
      url,
      ...option
    });
  };

  /**
   * 返回到上一页
   * @param delta {number}
   */
  const go = (delta: number) => {
    Taro.navigateBack({
      delta: -delta
    });
  };

  return {
    push,
    go,
    redirect
  };
};
