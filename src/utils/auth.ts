import { getStorageData, removeStorageData, setStorageData } from '.';

const TOKEN_KEY = 'token';

export const getToken = () => {
  return getStorageData(TOKEN_KEY);
};

export const setToken = (token: string) => {
  return setStorageData(TOKEN_KEY, token, true);
};

export const removeToken = () => {
  return removeStorageData(TOKEN_KEY);
};
