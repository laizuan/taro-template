export interface TabbarRoute {
  routeName: string;
  title: string;
  icon: string;
  url: string;
  fontClassName?: string;
  classPrefix?: string;
  img?: string;
  activeImg?: string;
  href?: string;
  to?: string | StringObject;
  num?: number;
}
